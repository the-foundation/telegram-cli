FROM alpine
LABEL Maintainer="Main Tainer <anyuser@local.lan>" Name=telegram-cli Version=1.4.1

##RUN apk add --no-cache --virtual build-dependencies  build-base git  jansson-dev  libconfig-dev libgcrypt-dev  libevent-dev  make  openssl-dev  readline-dev  zlib-dev  && cd /tmp && git clone --recursive https://github.com/vysheng/tg.git     && cd tg   && ./configure --disable-openssl --prefix=/usr CFLAGS="$CFLAGS -w" --disable-liblua     && make  -j $(nproc)    && cp /tmp/tg/bin/* /usr/local/bin     && rm -rf /tmp/tg     && rm -rf /var/lib/apt/lists/* || true  && apk del build-dependencies

## DANGER 
RUN apk add --no-cache --virtual build-dependencies  build-base git  jansson-dev  libconfig-dev libgcrypt-dev libevent-dev  make  openssl-dev  readline-dev  zlib-dev  && cd /tmp && git clone --recursive https://github.com/vysheng/tg.git     && cd tg   && ./configure --prefix=/usr CFLAGS="-w" --disable-liblua     && make  -j $(nproc)    && cp /tmp/tg/bin/* /usr/local/bin     && rm -rf /tmp/tg     && rm -rf /var/lib/apt/lists/* || true  && apk del build-dependencies

RUN which telegram-cli
RUN apk add --no-cache  jansson  libconfig libgcrypt libcrypto1.0  libevent  libssl1.0  readline     && rm -rf /var/lib/apt/lists/* || true 
ARG USER=user
ARG UID=1000
ENV USER=user
RUN UID=$UID /bin/sh -c "mkdir /home/$USER   && addgroup -g $UID -S $USER   && adduser -u $UID -D -S -G $USER $USER   && chown -R $USER:$USER /home/$USER"
USER user

VOLUME /home/$USER/.telegram-cli
ENTRYPOINT "/usr/local/bin/telegram-cli"
